// Funciones relacionadas con la comunicación
import hypermedia.net.*;
import processing.net.*;


UDP bdlisten;    // define the UDP object
UDP player;
UDP master;

String masterIp      = "localhost"; // Ip del master
String udpMsg        = "";          // Message from the other (master or player)
String mySubnet      = "";          // La subnet en la que opero (me sirve si soy master)

static String myIpAddress   = "";   // mi Ip

int     myNumber     = 0;           // Mi número de jugador, que coincidirá con mi último dígito de la IP

long lastMsgTime;  // lleva cuenta del último mensaje del master
                   // con la intención de convertirme en master 
                   // si el último master se fue del juego
                        
void setupUdp() {
  // Inicialmente solo escuchamos por el puerto de recepción del anuncio del juego
  // Cuando empiece el juego ya abriremnos otros puertos para comunicarnos 
  bdlisten = new UDP( this, BDCSTR_RECV_PORT ); 
  bdlisten.listen( true );
  // Manera cutre de pillar mi IP (no sé cómo hacerlo con la biblioteca UPD)  
  myIpAddress = Server.ip();
  // Para hacer el broadcast, necesito los primeros tres dígitos de mi IP
  String parts[] = myIpAddress.split("\\.");
  mySubnet = parts[0] + "." + parts[1] + "." + parts[2]; // Mi subnet xxx.xxx.xxx.
  myNumber = Integer.parseInt(parts[3]);  // Mi Id ---.---.---.xxx  << la xxx
}

void send(String msg) {
  if (iAmTheMaster) {
    //println("Master manda " + msg); 
    String ip = mySubnet + ".255";         // the broadcast address of the subnet I am in
    master.send( msg, ip, PLAYER_PORT );   // sending to the players receiving port
  } else {
    //println("Player manda " + msg); 
    player.send( msg, masterIp, MASTER_PORT ); // Sending to the master receiving port  
  }
}

void createMaster() {
  bdlisten.close();                      // Ya no necesito escuchar anuncios, porque voy a ser master
  announcer = new Broadcaster();         // Creamos un anunciante del juego
  announcer.start();                     // Creamos el hilo de ejecución para el anuncio del juego
  master = new UDP( this, MASTER_PORT ); // Creamos un puerto de master para hablar con los jugadores
  master.broadcast(true);                // que admite broadcast
  master.listen(true);                   // y por el que también escuchamos
  iAmTheMaster = true;                   // y me pongo como master
  playersDb = new ArrayList<Player>();   // creo la base de datos de jugadores
  playersDb.add( new Player(myIpAddress, "" + myNumber) ); // y por simplicidad para otras cosas, me apunto en ella
                                                           // (al fin y al cabo, yo también juego)
  myNumber = 255;
}

void createPlayer() {
  bdlisten.close();                      // Ya no necesito escuchar anuncios, porque estoy en el juego
  player = new UDP( this, PLAYER_PORT ); // Abro un puerto de jugador por el que hablaré
  player.listen(true);                   // y por el que escucho al master
  String name = "" + myNumber;           // por ahora, mi nombre es mi ip (aunque no vale si juego dos copias en la misma máquina)
  send("I_AM_" + name);                  // Informo al master de que estoy en el juego y de quién soy
  lastInfoMsg = "Esperando al Juego...";
}

void receive( byte[] data, String ip, int port ) { 
  lastDbgMsg = "Received " + new String(data) + "\nfrom " + ip + "\nport " + port; // mensaje para depuración
  
  udpMsg = new String(data); // Convertimos la tira de bytes a un string
  
  // Si recibimos mensaje por el puerto de anuncio del juego
  if (port == BDCSTR_SEND_PORT) {
    switch(udpMsg) {      
      case "PLAYING":
        if (!isPlaying) {
          masterIp    = ip;       // Este es el  IP del maestro que está enviado el mensaje de anuncio
          masterFound = true;     // Hemos encontrado un server (maestro del juego)
          lastInfoMsg = "Wait for current \nplay to finish";
        }
        break;
      default:
        masterIp    = ip;       // Este es el  IP del maestro que está enviado el mensaje de anuncio
        masterFound = true;     // Hemos encontrado un server (maestro del juego)
        break;
    }
    lastMsgTime = millis(); // Lo usaremos para saber si el master se ha retirado del juego
  }
  
  // Si recibimos mensaje desde el master (esto sólo le pasa a los jugadores)
  if (port == MASTER_PORT) {
    if (udpMsg.charAt(0) == 'E') { // El master me ha dicho END
      bolas.clear();  // Borro las bolas que quedan
      isPlaying = false;
    }
    if (udpMsg.charAt(0) == 'S') { // El master me ha dicho START
      println("Go to play...");
      randomSeed(0);
      isPlaying = true;
    }
    if (udpMsg.charAt(0) == 'K') { // El master me informa de que han matado a la bola Kxxxx
      int ballId = Integer.parseInt(udpMsg.substring(1, udpMsg.length())); // Pillo el número de identificación
      kill(ballId);
    }
    if (udpMsg.charAt(0) == 'P') { // El master me dice que el jugador xxx tiene yyyy puntos (Pxxx:yyyy)
      String parts[] = (udpMsg.substring(1, udpMsg.length())).split(":"); // quitando la P, lo parto en xxx e yyyy
      playersPoints.put(parts[0], parts[1]);
    }
    lastMsgTime = millis(); // Lo usaremos para saber si el master se ha retirado del juego
  }
  
  // Si recibimos mensaje desde el player (esto sólo le pasa al master)
  if (port == PLAYER_PORT) {
    // Miro si empieza por "I"
    if (udpMsg.charAt(0) == 'I') { // El jugador me dice que está en el juego "I_AM_xxxx"
      String parts[] = udpMsg.split("_");
      playersDb.add( new Player(ip, parts[2]) ); // Añado un jugador con su ip y su nombre (número)
    } else { 
      // Si no, solo puede ser una coordenada (no se mandan más tipos de mensajes)
      String coords[] = udpMsg.split(":"); // Pilla las coordenadas donde me dicen que han pulsado
      if (coords.length == 2) { // por si acaso 
        int pmX = Integer.parseInt(coords[0]);
        int pmY = Integer.parseInt(coords[1]);
        for(Bola b : bolas ) {
          if (b.coordIsOver(pmX, pmY)) {
            int po = b.kill(); // mato la bola y miro los puntos que me da
            for(Player pl : playersDb) { // Busco entre los jugadores apuntados 
              if (pl.getIp().equals(ip)) {  // y si conicide su ip con la del mensaje
                pl.addPoints(po); // le apunto los puntos a este jugador
              }
            }
            informKilling(b); // y les digo a todos que ha muerto la bola b
          }
        }
      }
    }
  } 
}

// Busco un juego activo
boolean lookingForGame() {
  // Si ya he encontrado el master, o yo lo soy, me vuelvo
  if (iAmTheMaster || gameCreated) return false; 
  
  // si no encuentro un master, lo espero durante SEARCH_TIMEOUT milisegundos
  // (el proceso de escucha de udp actualizará la variable masterFound)
  if (!masterFound && millis() < timeOut) {
    lastInfoMsg = "Buscando Master";
    return true; 
  }
  
  // Si pasado el tiempo de espera no encontré un master, lo creo
  if (!masterFound) {
    createMaster(); // Creamos al master y su puerto de comunicación
    lastInfoMsg = "Master Creado...\nClick para empezar";
  } else {
    createPlayer(); // Creamos al jugador y su puerto de comunicación
    lastInfoMsg = "Juego encontrado.\nEsperando a comenzar...";
  }
  gameCreated = true; // El juego está en marcha
  return false; // Ya no buscamos más
}