class Player {
  String ip;    // La ip del jugador
  String name;  // su nombre
  int points;   // y los puntos que lleva
  
  
  Player(String _ip, String _n) {
    ip      = _ip;
    points  =   0;
    name    =  _n;
  }

  void setName(String n) {
    name = n;
  }
  
  void addPoints(int p) {
    points += p;
  }
  
  int getPoints() {
    return points;
  }

  String getIp() {
    return ip;
  }
  
  String getName() {
    return name;
  }
  
}