// Funciones relacionadas con la interacción con el usuario
void keyPressed() {
  if (iAmTheMaster) {
    if (key == ' ') {     // Para test, si pulso barra, finalizo el juego
      isPlaying = false;
      bolas.clear();         // Borro las bolas que quedan
      playersPoints.clear(); // Y los puntos de los jugadores
      send("END");           // y mando END
    } 
  }
}

void mouseClicked() {
  println("Playing:" + isPlaying + "   Master:" + iAmTheMaster);
  if (!isPlaying) {
    // Si no estamos en juego y somos el master
    if (iAmTheMaster) {
      // nos ponemos en juego
      isPlaying = true;
      randomSeed(0);
      // e informamos a los demás de que comenzamos el juego
      send("START");
    }
  } else {
    // Si soy el master, miro si he matado algo e informo
    if (iAmTheMaster) {
      // Si estoy jugando
      if (isPlaying) {
        // Otra forma muy cómoda de recorrer una lista en un ArrayList
        // en vez de for(int i = 0; i < bolas.size(); i++) { Bola b = bolas.get(i); ...
        for(Bola b : bolas) {
          if (b.mouseIsOver()) {
            informKilling(b);
            myPoints += b.kill();
          }
        } 
      } else {
        // si no, paso a jugar
        isPlaying = true;
      } 
    } else {
      // Si soy un jugador normal, le digo al master dónde he pulsado
      send(mouseX + ":" + mouseY);
    }
  }
}