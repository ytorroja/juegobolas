String  lastInfoMsg = "";
String  lastDbgMsg = "";
long  nextPointsInfoTime;

void setup() {
  size(300,  300, P2D);
  smooth();
  frameRate(60);
  randomSeed(0);
  textSize(12);
  
  setupUdp();
  setupGame();
}

void draw() {  
  String  theScore = "";
  
  if (lookingForGame()) { // buscando un juego
    background(SEARCHING_BGD_COLOR);
  } else {
    if (isPlaying) { // Estamos jugando
      background(PLAYING_BCG_COLOR);
      ballCreator(PERCENT);
      paintBalls();
      updateBalls();
      removeDeadBalls();
      if (iAmTheMaster) {
        if (millis() > nextPointsInfoTime)  {
          sendPoints();
          nextPointsInfoTime += 1000; // Volveré a informar en 1 segundo
        }
        for( Player pl : playersDb ) {
          theScore += "\n" + pl.getName() + ":" + pl.getPoints();
        }
        
      } else {
        for(HashMap.Entry<String, String> e : playersPoints.entrySet()) {
          theScore += "\n" + e.getKey() + ":" + e.getValue();
        }
      }
    } else { 
      // Estamos esperando a que el pesao del master decida iniciar el juego
      background(WAITING_BGD_COLOR);
    }
  }
  
  // checkMasterAlive(); // El master puede marcharse del juego
                      // entonces alguien tiene que convertirse en master

  fill(0);
  String theText = (iAmTheMaster ? "Master\n" : "Player\n") + lastInfoMsg + "\n" + lastDbgMsg;
  text(theScore + "\n" + theText, 10, 20);
}

void sendPoints() {
  for(Player b : playersDb ) {
    send("P" + b.getName() + ":" + b.getPoints()); // Envio los puntos de este jugador
  }
}