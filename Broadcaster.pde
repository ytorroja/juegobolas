// Esta clase tiene como objeto mantener un 
// hilo de ejecución donde el Master informa de
// forma periódica que está vivo y del estado del juego
Broadcaster announcer;

class Broadcaster extends Thread {
  UDP bdcaster;
  
  Broadcaster() {
    bdcaster = new UDP( this, BDCSTR_SEND_PORT ); // Creamos un puerto de anuncio
    bdcaster.broadcast(true);    // que permita broadcast
  }
  
  public void run() {
    while(true) {
      // Cada segundo decimos, al menos, "hola"
      announceGame(); 
      delay(1000);
    }
  }
  
  void announceGame() {
    String ip = mySubnet + ".255";            // the broadcast address of the subnet I am in
    if (isPlaying) {
      // Por si alguien se apunta tarde anunciamos que ya estamos jugando
      bdcaster.send( "PLAYING", ip, BDCSTR_RECV_PORT );    // sending to the players receiving port 
    } else {
      // Y si no,pues damos la bienvenida al juego
      bdcaster.send( "HELLO", ip, BDCSTR_RECV_PORT );    // sending to the players receiving port 
    }
  }

}