class Bola {
  
  int id;           // identificador único de la bola
  
  float r;          // radio
  float x, y;       // posición
  float velX, velY; // velocidad
  int dieTime;      // Hora programada de suicidio
  int c;            // Color de relleno
  int s;            // Color de borde
  color co;

  // El estado muriendo es para que la bola haga un fadeout suave al desaparecer
  int state;  // Estado -> 0 muerto,  1 vivo,  2 muriendo 
  static final int DEAD  = 0;
  static final int ALIVE = 1;
  static final int DYING = 2;

  Bola() {
    id = lastBall++;
    r = random(MIN_R, MAX_R);
    x = random( 2 * r, width - 2 * r); 
    y = random( 2 * r, height - 2 * r); 
    velX = random(-MAX_VEL, MAX_VEL); 
    velY = random(-MAX_VEL, MAX_VEL); 
    dieTime = millis() + (int)random(MIN_T_MS, MAX_T_MS);
    c = color(random(255), random(255), random(255), 80);
    s = c & 0x00ffffff | 0x80000000;
    state = 1;
  }
  
  // Calcula los colores de fondo y borde de la bola 
  // de forma progersiva en función de un parámetro de 0 a 1
  void fade(float f) {
    int ca = (c & 0xFF000000) >> 24;
    c = ((int)(ca * f) << 24) | (c & 0x00FFFFFF);
    int cs = (c & 0xFF000000) >> 24;
    s = ((int)(cs * f) << 24) | (s & 0x00FFFFFF);
  }
  
  // Actualiza el estado de la bola
  void update() {
    x = x + velX;
    y = y + velY;
    if (x + r > width && velX > 0) velX *= -1;
    if (x - r < 0 && velX < 0) velX *= -1;
    if (y + r > height && velY > 0) velY *= -1;
    if (y - r < 0 && velY < 0) velY *= -1;
    if (millis() > dieTime - FADE_TIME) state = DYING;
    if (millis() > dieTime) state = DEAD;
  }
  
  // Dibuja la bola
  void draw() {
    if (state >= DYING) fade(((float)(dieTime - millis()))/FADE_TIME);
    if (state != DEAD) {
      pushStyle();
      stroke(s);
      fill(c);
      ellipse(x, y, 2*r, 2*r);
      fill(0);
      //text(dieTime - millis(), x, y);
      popStyle();
    }
  }
  
  // Devuelve si la posición dada está sobre esta bola 
  boolean coordIsOver(int _x, int _y) {
    if (dist(x, y, _x, _y) < r) return true;
    return false;
  }
  
  // Devuelve si el ratón está sobre esta bola
  boolean mouseIsOver() {
    return coordIsOver(mouseX, mouseY);
  }

  // Esta función programa al la bola para morir, y devuelve los puntos
  // asociados a esta muerte (depende del tamaño de la bola y de la velocidad)
  int kill() {
    if (state == ALIVE) dieTime = millis() + FADE_TIME;
    float v = dist(0, 0, velX, velY);
    return (int)(map(r, MIN_R, MAX_R, 10, 1) + map(v, 0, MAX_VEL, 1, 10));
  }
  
  // Devuelve en qué estado está la bola
  int getState() {
    return state;
  }
}