final int MIN_R = 10;        // Radio mínimo
final int MAX_R = 50;        // Radio máximo
final int MIN_T_MS = 5000;   // Tiempo de vida mínimo
final int MAX_T_MS = 10000;  // Tiempo de vida máximo
final int MAX_VEL = 1;       // Velocidad máxima
final int FADE_TIME = 2000;  // Tiempo de desaparición suave de la bola
final int PERCENT = 4;       // Frecuencia de aparición de la nuevas bolas

final int SEARCH_TIMEOUT    = 5000;  //  Número de ms de espera para buscar un juego activo
final int NO_MASTER_TIMEOUT = 5000;  //  Número de ms de espera para buscar un nuevo master

final int BDCSTR_SEND_PORT = 6000;  // Puerto de recepción para anuncio del juego
final int BDCSTR_RECV_PORT = 6001;  // Puerto de broadcast para anuncio del juego

final int MASTER_PORT = 6100;       // Puerto de comunicación del maestro del juego
final int PLAYER_PORT = 6200;       // Puerto de comunicación de los jugadores

final int SEARCHING_BGD_COLOR = 0xFFE56D6D;
final int WAITING_BGD_COLOR   = 0xFFE5E56D;
final int PLAYING_BCG_COLOR   = 0xFFFFFFFF;