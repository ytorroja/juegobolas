// Funciones relacionadas con la lógica de juego

boolean masterFound   = false; // Flag que dice si hemos encontrado un master en la red
boolean gameCreated   = false; // Flag que indica si el juego existe (existe un master)
boolean iAmTheMaster  = false; // Flag que dice si soy master
boolean isPlaying     = false; // Flag que dice si estamos jugando

int     lastBall = 0;  // contador para generar un número único asociado a cada bola (se incrementa con cada creación)

int     myPoints    = 0;  // Puntos acumulados  
String  myName      = ""; // Nombre del jugador

long    timeOut;

ArrayList<Bola>   bolas;
ArrayList<Player> playersDb;

HashMap<String, String> playersPoints;

// Setup de las bolas
void setupGame() {
  bolas = new ArrayList<Bola>();
  playersPoints = new HashMap<String, String>();
  timeOut = millis() + SEARCH_TIMEOUT;
}

// Crea bolas aleatoriamente en función de una 
// probabilidad dada (cuanto más baja, menos bolas aparecen)
void ballCreator(float percent) {
  if (random(100) < percent) {
    bolas.add(new Bola());
  }
}

// Pinta las bolas
void paintBalls() {
  for(Bola b : bolas) b.draw();
}

// Actualiza el estado de las bolas
void updateBalls() {
  for(Bola b : bolas) b.update();
}

// Elimina las bolas muertas
void removeDeadBalls() {
  for(int i = bolas.size() - 1; i >= 0; i--) {
    Bola b = bolas.get(i);
    // Borra la bola de la lista de bolas
    if (b.getState() == Bola.DEAD) bolas.remove(i);
    // En java, si un objeto ya no está referenciado es como si se borrase. 
    // Es decir, si la bola ya no está referenciada en ningún sitio, el
    // espacio de memoria que ocupaba, puede ser usado por otro objeto
  }
}

// Informo a los jugadores que esta bola ha muerto
void informKilling(Bola b) {
  send("K" + b.id); // Informo a todos de que ha muerto la bola id
}

void kill(int n) {
   for(Bola b : bolas) {
     if (b.id == n) b.kill();
   }
}

// Check de que el master sigue en juego
void checkMasterAlive() {
  if (!iAmTheMaster) {
    if (millis() > lastMsgTime + SEARCH_TIMEOUT) {
      masterFound = false;
      gameCreated = false;
    }
  }
}